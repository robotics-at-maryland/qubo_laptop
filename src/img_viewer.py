import rclpy
from rclpy.node import Node
from rclpy.qos import qos_profile_sensor_data
qos_pf = qos_profile_sensor_data
#qos_pf.depth = 1

from sensor_msgs.msg import Image

import cv2
from cv_bridge import CvBridge
import numpy as np
import matplotlib.pyplot as plt


class ImgSubscriber(Node):

    def __init__(self):
        super().__init__('img_viewer')
        self.subscription = self.create_subscription(
            Image,
            '/mako/image',
            self.listener_callback,
            qos_profile=qos_pf)
        self.subscription  # prevent unused variable warning
        self.bridge = CvBridge()

    def listener_callback(self, msg):
        print("here")
        img = self.bridge.imgmsg_to_cv2(msg)
#        img = cv2.resize(img, (1280, 720))
        cv2.imshow("img",img[:,:,::-1])
        cv2.imwrite("./color_img.jpg", img)

        cv2.waitKey(50)


def main(args=None):
    rclpy.init(args=args)

    img_subscriber = ImgSubscriber()

    rclpy.spin(img_subscriber)

    # Destroy the node explicitly
    # (optional - otherwise it will be done automatically
    # when the garbage collector destroys the node object)
    img_subscriber.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()
