#!/usr/bin/env python3

import rclpy
from rclpy.node import Node

from inputs import get_gamepad
from geometry_msgs.msg import WrenchStamped
from std_msgs.msg import Int32, UInt8MultiArray
import math
import threading
import numpy as np

class XboxController(object):
    MAX_TRIG_VAL = math.pow(2, 8)
    MAX_JOY_VAL = math.pow(2, 15)

    def __init__(self):

        self.LeftJoystickY = 0
        self.LeftJoystickX = 0
        self.RightJoystickY = 0
        self.RightJoystickX = 0
        self.LeftTrigger = 0
        self.RightTrigger = 0
        self.LeftBumper = 0
        self.RightBumper = 0
        self.A = 0
        self.X = 0
        self.Y = 0
        self.B = 0
        self.LeftThumb = 0
        self.RightThumb = 0
        self.Back = 0
        self.Start = 0
        self.LeftDPad = False
        self.RightDPad = False
        self.UpDPad = False
        self.DownDPad = False
        self.pitchMax = 0.8
        self.rollMax = 0.8
        self.yawMax = 0.6
        self.linXMax = 1.4
        self.linYMax = 1.4
        self.linZMax = 1.4
        self.thrustMax = [self.linXMax, self.linYMax, self.linZMax, self.rollMax, self.pitchMax, self.yawMax]
        self.thrustMin = 0.001

        self.torpedo_pos = 1
        self.led_mode = 2

        self._monitor_thread = threading.Thread(target=self._monitor_controller, args=())
        self._monitor_thread.daemon = True
        self._monitor_thread.start()

    # Apply a cubic scaling to the input value. Makes precise movements on the
    # joysticks possible, while still allowing full throttle.
    @staticmethod
    def scale(x):
        return x * x * x

    def read(self): # return the buttons/triggers that you care about in this method
        # Both joysticks have X right positive, Y down positive
        x = -self.LeftJoystickY
        y = -self.LeftJoystickX
        z = 0 if self.A and self.B else 1 if self.A else -1 if self.B else 0
        roll = self.RightJoystickX
        pitch = -self.RightJoystickY
        yaw = 0 if self.LeftTrigger > 0.5 and self.RightTrigger > 0.5 else 1 if self.LeftTrigger > 0.5 else -1 if self.RightTrigger > 0.5 else 0
        thrust_arr = [x, y, z, roll, pitch, yaw]
        thrust_arr = map(XboxController.scale, thrust_arr)
        thrust_arr = [thrust * max for thrust, max in zip(thrust_arr, self.thrustMax)]
        thrust_arr = np.clip(thrust_arr, [-max for max in self.thrustMax], self.thrustMax)
        thrust_arr = list(map(lambda thrust: thrust if abs(thrust) > self.thrustMin else 0, thrust_arr))

        if self.X:
            self.torpedo_pos = 0
            self.led_mode = 4
        elif self.Y:
            self.torpedo_pos = 2
            self.led_mode = 4
        else:
            self.torpedo_pos = 1
            self.led_mode = 2

        return thrust_arr, self.torpedo_pos, self.led_mode, self.DownDPad, self.Start, self.Y, self.X


    def _monitor_controller(self):
        while True:
            events = get_gamepad()
            for event in events:
                if event.code == 'ABS_Y':
                    self.LeftJoystickY = event.state / XboxController.MAX_JOY_VAL # normalize between -1 and 1
                elif event.code == 'ABS_X':
                    self.LeftJoystickX = event.state / XboxController.MAX_JOY_VAL # normalize between -1 and 1
                elif event.code == 'ABS_RY':
                    self.RightJoystickY = event.state / XboxController.MAX_JOY_VAL # normalize between -1 and 1
                elif event.code == 'ABS_RX':
                    self.RightJoystickX = event.state / XboxController.MAX_JOY_VAL # normalize between -1 and 1
                elif event.code == 'ABS_Z':
                    self.LeftTrigger = event.state / XboxController.MAX_TRIG_VAL # normalize between 0 and 1
                elif event.code == 'ABS_RZ':
                    self.RightTrigger = event.state / XboxController.MAX_TRIG_VAL # normalize between 0 and 1
                elif event.code == 'BTN_TL':
                    self.LeftBumper = event.state
                elif event.code == 'BTN_TR':
                    self.RightBumper = event.state
                elif event.code == 'BTN_SOUTH':
                    self.A = event.state
                elif event.code == 'BTN_NORTH':
                    self.X = event.state
                elif event.code == 'BTN_WEST':
                    self.Y = event.state
                elif event.code == 'BTN_EAST':
                    self.B = event.state
                elif event.code == 'BTN_THUMBL':
                    self.LeftThumb = event.state
                elif event.code == 'BTN_THUMBR':
                    self.RightThumb = event.state
                elif event.code == 'BTN_SELECT':
                    self.Back = event.state
                elif event.code == 'BTN_START':
                    self.Start = event.state
                # elif event.code == 'BTN_TRIGGER_HAPPY1':
                #     self.LeftDPad = event.state
                # elif event.code == 'BTN_TRIGGER_HAPPY2':
                #     self.RightDPad = event.state
                # elif event.code == 'BTN_TRIGGER_HAPPY3':
                #     self.UpDPad = event.state
                # elif event.code == 'BTN_TRIGGER_HAPPY4':
                #     self.DownDPad = event.state
                ## BLOCK: PS4 DPad.
                # Though also these are the events raised for at least Alex's
                # Xbox controller. Not sure if the NBRF Xbox controller is
                # different.
                elif event.code == 'ABS_HAT0Y': # vertical
                    if event.state == -1:
                        self.UpDPad = True
                        self.DownDPad = False
                    elif event.state == 1:
                        self.UpDPad = False
                        self.DownDPad = True
                    else: # neither pressed
                        self.UpDPad = False
                        self.DownDPad = False
                elif event.code == 'ABS_HAT0X': # horizontal
                    if event.state == -1:
                        self.LeftDPad = True
                        self.RightDPad = False
                    elif event.state == 1:
                        self.LeftDPad = False
                        self.RightDPad = True
                    else: # neither pressed
                        self.LeftDPad = False
                        self.RightDPad = False
                ## END BLOCK: PS4 DPad

class TeleopControlPublisher(Node):

    def __init__(self, name, channel='/desired_thrust/add', torpedo_channel = 'torpedo_trigger', led_channel = '/led_mode', pneumatics_channel='desired_solenoid'):
        super().__init__(name)
        self.publisher_ = self.create_publisher(WrenchStamped, channel, 10)
        self.torpedo_publisher_ = self.create_publisher(Int32, torpedo_channel, 10)
        self.led_publisher_ = self.create_publisher(Int32, led_channel, 10)
        self.pneumatics_publisher_ = self.create_publisher(UInt8MultiArray, pneumatics_channel, 10)

    def publish(self, msg):
        self.publisher_.publish(msg)

    def torpedo_publish(self, msg):
        self.torpedo_publisher_.publish(msg)

    def led_publish(self, msg):
        self.led_publisher_.publish(msg)

    def pneumatics_publish(self, msg):
        self.pneumatics_publisher_.publish(msg)

    def get_ros_msg(self, thrust_arr):
        currentTeleopMsg = WrenchStamped()
        currentTeleopMsg.header.frame_id = "teleop"
        currentTeleopMsg.header.stamp = self.get_clock().now().to_msg()
        currentTeleopMsg.wrench.force.x = float(thrust_arr[0])
        currentTeleopMsg.wrench.force.y = float(thrust_arr[1])
        currentTeleopMsg.wrench.force.z = float(thrust_arr[2])
        currentTeleopMsg.wrench.torque.x = float(thrust_arr[3])
        currentTeleopMsg.wrench.torque.y = float(thrust_arr[4])
        currentTeleopMsg.wrench.torque.z = float(thrust_arr[5])
        return currentTeleopMsg


if __name__ == '__main__':
    # logging.basicConfig(level=logging.DEBUG)
    joy = XboxController()
    rclpy.init()
    pub = TeleopControlPublisher("TeleopControlNode")
    while rclpy.ok():
        thrust_arr, torpedo_pos, led_mode, down_dpad, start, y_button, x_button = joy.read()
        if thrust_arr is not None:
            print(thrust_arr)
            arrMsg = pub.get_ros_msg(thrust_arr)
            pub.publish(arrMsg)
        if torpedo_pos is not None:
            torpedo_msg = Int32()
            torpedo_msg.data = torpedo_pos
            pub.torpedo_publish(torpedo_msg)
        if led_mode is not None:
            led_msg = Int32()
            led_msg.data = led_mode
            pub.led_publish(led_msg)
        if down_dpad is not None \
        or start is not None \
        or y_button is not None \
        or x_button is not None:
            mdropper = 1 if down_dpad else 0
            claw = 1 if start else 0
            left_torpedo = 1 if x_button else 0
            right_torpedo = 1 if y_button else 0
            msg = UInt8MultiArray()
            msg.data = [mdropper, left_torpedo, right_torpedo, claw]
            pub.pneumatics_publish(msg)
