import rclpy
from rclpy.node import Node
from rclpy.qos import qos_profile_sensor_data
qos_pf = qos_profile_sensor_data
#qos_pf.depth = 1

from sensor_msgs.msg import CompressedImage

import cv2
from cv_bridge import CvBridge
import numpy as np


class ImgSubscriber(Node):

    def __init__(self):
        super().__init__('depth_viewer')
        self.subscription = self.create_subscription(
            CompressedImage,
            '/zed_depth_compressed',
            self.listener_callback,
            qos_profile=qos_pf)
        self.subscription  # prevent unused variable warning
        self.bridge = CvBridge()

    def listener_callback(self, msg):
        img = self.bridge.compressed_imgmsg_to_cv2(msg)
        new_img = np.ones((img.shape[0], img.shape[1], 3), dtype = np.uint8) * 200 
        new_img[:,:,0] = img // 2

        cv2.imshow("Zed Img", cv2.cvtColor(new_img, cv2.COLOR_HSV2BGR))
        cv2.waitKey(1)


def main(args=None):
    rclpy.init(args=args)

    img_subscriber = ImgSubscriber()

    rclpy.spin(img_subscriber)

    # Destroy the node explicitly
    # (optional - otherwise it will be done automatically
    # when the garbage collector destroys the node object)
    img_subscriber.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()
