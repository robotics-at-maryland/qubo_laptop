import numpy as np
import cv2
import matplotlib.pyplot as plt

def remove_nan_smooth(img):

    while(True):
        inf_indices = np.argwhere(np.isinf(img[1:-1,1:-1]))
        print(len(inf_indices))

        if(len(inf_indices) == 0):
            break

        indices_stack = np.tile(inf_indices, (9,1,1))
        
        for i in range(3):
            for j in range(3):
                index = 3 * i + j
                indices_stack[index, :, 0] += i - 1
                indices_stack[index, :, 1] += j - 1
        
        taken_values = img[indices_stack[:,:,0] + 1, indices_stack[:,:,1] + 1]
        not_infs = np.invert(np.isinf(taken_values))
        replaceable_indexes = np.any(not_infs, axis=0)
        replaceable_pixels = indices_stack[4][replaceable_indexes] #get the middle pixel indices
        sum_len = np.sum(not_infs, axis=0)
        sums = np.sum(np.nan_to_num(taken_values, nan=0, posinf=0, neginf=0), axis=0)
        img[replaceable_pixels[:, 0] + 1, replaceable_pixels[:, 1] + 1] = sums[replaceable_indexes] / sum_len[replaceable_indexes]

    top_row_infs = np.argwhere(np.isinf(img[0])) 
    img[0, top_row_infs] = img[1, top_row_infs]

    bottom_row_infs = np.argwhere(np.isinf(img[-1])) 
    img[-1, bottom_row_infs] = img[-2, bottom_row_infs]

    left_col_infs = np.argwhere(np.isinf(img[:,0])) 
    img[left_col_infs, 0] = img[left_col_infs, 1]

    right_col_infs = np.argwhere(np.isinf(img[:,-1])) 
    img[right_col_infs, 0] = img[right_col_infs, 1]
    return img

def draw_lines(img, lines, color = (0,255,0)):
    first = True
    for r_theta in lines:
        arr = np.array(r_theta[0], dtype=np.float64)
        r, theta = arr
        a = np.cos(theta)
        b = np.sin(theta)
        x0 = a*r
        y0 = b*r
        x1 = int(x0 + 1000*(-b))
        y1 = int(y0 + 1000*(a))
        x2 = int(x0 - 1000*(-b))
        y2 = int(y0 - 1000*(a))
        if first:
            cv2.line(img, (x1, y1), (x2, y2), color, 2)
            first = False
        else:
            cv2.line(img, (x1, y1), (x2, y2), (255,255,255), 2)
    return img

def vertLineDetectP(img, blur=10, nms_length = 5, thresh = 200, min_length = 20, max_gap = 20):

    blurred_img = cv2.blur(img, (blur,blur))
    kernel1 = np.array([[-1, 0, 1]])
    hori_img_grad = cv2.filter2D(blurred_img, ddepth=-1, kernel=kernel1)
    nms_img = np.zeros((hori_img_grad.shape[0],hori_img_grad.shape[1],nms_length))
    widthNMS = hori_img_grad.shape[1] - 2 * (nms_length//2)
    for i in range(nms_length):
        x = i - nms_length//2
        nms_img[:,(nms_length//2) : (nms_length//2) + widthNMS, i] = hori_img_grad[:,x + (nms_length//2) : x + (nms_length//2) + widthNMS]
    
    max_arg = np.argmax(nms_img, axis=2)
    bool_edges_right = np.logical_and((max_arg == nms_length//2), (hori_img_grad > thresh)).astype(np.uint8)

    min_arg = np.argmin(nms_img, axis=2)
    bool_edges_left = np.logical_and((min_arg == nms_length//2), (hori_img_grad < -thresh)).astype(np.uint8)

    dilation_kernel = np.ones((3,3), np.uint8)
    bool_edges_left = cv2.dilate(bool_edges_left, dilation_kernel, iterations=1)
    bool_edges_right = cv2.dilate(bool_edges_right, dilation_kernel, iterations=1)


    right_lines = cv2.HoughLinesP(bool_edges_right,1,np.pi/120, min_length, max_gap)
    left_lines = cv2.HoughLinesP(bool_edges_left,1,np.pi/120, min_length, max_gap)

    cv2.imshow("bool edges right", bool_edges_right * 255)

    return left_lines, right_lines, hori_img_grad

def horiLineDetectP(img, blur=10, nms_length = 3, thresh = 100, min_length = 20, max_gap = 20):

    blurred_img = cv2.blur(img, (blur,blur))
    kernel1 = np.array([[-1], [0], [1]])
    vert_img_grad = cv2.filter2D(blurred_img, ddepth=-1, kernel=kernel1)
    nms_img = np.zeros((vert_img_grad.shape[0],vert_img_grad.shape[1],nms_length))
    widthNMS = vert_img_grad.shape[0] - 2 * (nms_length//2)
    for i in range(nms_length):
        x = i - nms_length//2
        nms_img[(nms_length//2) : (nms_length//2) + widthNMS,:, i] = vert_img_grad[x + (nms_length//2) : x + (nms_length//2) + widthNMS, :]
    
    max_arg = np.argmax(nms_img, axis=2)
    bool_edges_bottom = np.logical_and((max_arg == nms_length//2), (vert_img_grad > thresh)).astype(np.uint8)

    min_arg = np.argmin(nms_img, axis=2)
    bool_edges_top = np.logical_and((min_arg == nms_length//2), (vert_img_grad < -thresh)).astype(np.uint8)

    dilation_kernel = np.ones((3,3), np.uint8)
    bool_edges_bottom = cv2.dilate(bool_edges_bottom, dilation_kernel, iterations=1)
    bool_edges_top = cv2.dilate(bool_edges_top, dilation_kernel, iterations=1)

    bottom_lines = cv2.HoughLinesP(bool_edges_bottom,1,np.pi/120, min_length, max_gap)
    top_lines = cv2.HoughLinesP(bool_edges_top,1,np.pi/120, min_length, max_gap)

    #cv2.imshow("bool edges bottom", bool_edges_bottom * 255)

    return bottom_lines, top_lines, vert_img_grad

def check_hori_merge(l1, l2, xDist, yDist):
    line1 = l1[0]
    line2 = l2[0]

    if line1[0] > line2[0]:
        temp = line1
        line1 = line2
        line2 = temp

    minx1 = min(line1[0], line1[2])
    maxx1 = max(line1[0], line1[2])
    miny1 = min(line1[1], line1[3])
    maxy1 = max(line1[1], line1[3])
    minx2 = min(line2[0], line2[2])
    maxx2 = max(line2[0], line2[2])
    miny2 = min(line2[1], line2[3])
    maxy2 = max(line2[1], line2[3])

    vertCheck = abs(maxy2 - maxy1) < yDist or abs(maxy2 - miny1) < yDist or abs(miny2 - maxy1) < yDist or abs(miny2 - miny1) < yDist 

    horiCheck = (maxx1 - minx2 > -xDist) 
    merge = (vertCheck and horiCheck)
    line = None
    if merge:
        max2 = line2[2] > line1[2]
        xEnd = line1[2]
        yEnd = line1[3]
        if max2:
            xEnd = line2[2]
            yEnd = line2[3]

        line = [[line1[0], line1[1], xEnd, yEnd]]
    return merge, line

def group_hori_lines(lines, xDist = 20, yDist = 20):
    line_merged = True
    while line_merged:
        line_merged = False
        i = 0
        while i < len(lines):
            j = i+1
            while j < len(lines):
                merge, line = check_hori_merge(lines[i], lines[j], xDist, yDist)
                if merge:
                    line_merged = True
                    lines[i] = line
                    lines = np.delete(lines, j, axis=0)
                else:
                    j += 1
            i += 1
    return lines

def filter_hori_lines(lines, min_length = 200, max_slope = 0.3):
    i = 0
    while i < len(lines):
        run = abs(lines[i, 0, 0] - lines[i, 0, 2])
        rise = abs(lines[i, 0, 1] - lines[i, 0, 3])

        slope = rise/run

        length = ((lines[i, 0, 0] - lines[i, 0, 2])**2 + (lines[i, 0, 1] - lines[i, 0, 3])**2)**0.5
        if length > min_length and slope < max_slope:
            i += 1
        else:
            lines = np.delete(lines, i, axis=0)
    maxYLineIndex = 0
    maxY = 0

    #put lowest line in image (highst avg Y) first in Line to be our best gate guess
    for i in range(len(lines)):
        avgY =(lines[i,0,1] + lines[i,0,3]) / 2
        if avgY > maxY:
            maxY = avgY
            maxYLineIndex = i
    
    temp = np.copy(lines[0])
    lines[0] = lines[maxYLineIndex]
    lines[maxYLineIndex] = temp

    return lines

def check_gate_lines(tLine, bLine, img, xDist = 40, yDist = 60):
    if abs(tLine[0,0] - bLine[0,0]) < xDist and abs(tLine[0,2] - bLine[0,2]) < xDist:
        midX = (tLine[0,0] + bLine[0,0] + tLine[0,2] + bLine[0,2]) // 4
        topY = (((midX - tLine[0,0])/(tLine[0,2] - tLine[0,0])) * (tLine[0,3] - tLine[0,1])) + tLine[0, 1]
        bottomY = (((midX - bLine[0,0])/(bLine[0,2] - bLine[0,0])) * (bLine[0,3] - bLine[0,1])) + bLine[0, 1]

        if abs(bottomY - topY) > yDist:
            return None

        depth = np.median(img[(int)(topY):(int)(bottomY), midX])
        width = abs(((tLine[0,2] + bLine[0,2])/2) - ((tLine[0,0] + bLine[0,0])/2) )
        return [depth, width, midX, (bottomY + topY)/2]

    else:
        return None




def find_gates(bottom_lines, top_lines, img):
    gates = []
    for i in range(len(bottom_lines)):
        bottom_line = bottom_lines[i]
        for j in range(len(top_lines)):
            top_line = top_lines[j]
            gate = check_gate_lines(top_line, bottom_line, img)
            if gate is not None:
                gates.append(gate)
                break
    if len(gates) == 0:
        return None
    else:
        return gates




if __name__ == "__main__":
    img_num = 0
    while(True):
        key = -1
        img_path = "/home/josh/ros_ws/src/qubo_laptop/src/gate_images/{:06d}.npy".format(img_num)
        gate_thresh = 40
        with open(img_path, "rb") as f:
            img = np.load(f)
            img = np.nan_to_num(img, nan=0, posinf=0, neginf=0)#img = remove_nan_smooth(img)
            blurred_img = cv2.blur(img, (10,10))

            #left_lines, right_lines, grad_img = vertLineDetectP(img)
            bottom_lines, top_lines, grad_img = horiLineDetectP(img)
            merged_lines = group_hori_lines(bottom_lines)
            merged_top_lines = group_hori_lines(top_lines)
            filtered_hori_lines = filter_hori_lines(merged_lines)
            filtered_hori_top_lines = filter_hori_lines(merged_top_lines)

            gates = find_gates(filtered_hori_lines, filtered_hori_top_lines, img)

            img = cv2.cvtColor((img / np.max(img)) * 255, cv2.COLOR_GRAY2BGR)
            if gates is not None:
                gateDepth, gateWidth, gateX, gateY = gates[0]
                cv2.line(img, ((int)(gateX - gateWidth/2),(int)( gateY)), ((int)(gateX + gateWidth/2), (int)(gateY)), color=(255, 0, 0))
                print("gate depth is ", gateDepth)
                print("gate width is ", gateWidth)

            if filtered_hori_lines is not None:
                for i in range(len(filtered_hori_lines)):
                    if i == 0:
                        cv2.line(img, filtered_hori_lines[i, 0,0:2], filtered_hori_lines[i, 0,2:], color = (0,255,0), thickness = 4)
                    else:
                        cv2.line(img, filtered_hori_lines[i, 0,0:2], filtered_hori_lines[i, 0,2:], color = (0,0,255), thickness = 2)
            


            print("done lines")
            cv2.imshow("image", img.astype(np.uint8))

            while key is not ord("n"):
                key = cv2.waitKey(30)
                if key is ord("q"):
                    exit()
        img_num += 1