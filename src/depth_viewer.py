import rclpy
from rclpy.node import Node
from rclpy.qos import qos_profile_sensor_data
qos_pf = qos_profile_sensor_data
#qos_pf.depth = 1

from sensor_msgs.msg import Image

import cv2
from cv_bridge import CvBridge
import numpy as np

import matplotlib.pyplot as plt


class ImgSubscriber(Node):

    def __init__(self, record_image_folder):
        super().__init__('depth_viewer')
        self.subscription = self.create_subscription(
            Image,
            '/zed_depth',
            self.listener_callback,
            qos_profile=qos_pf)
        self.subscription  # prevent unused variable warning
        self.bridge = CvBridge()
        self.record_image_folder = record_image_folder
        self.record_images = (record_image_folder != None)
        self.i = 0

    def listener_callback(self, msg):
        img = self.bridge.imgmsg_to_cv2(msg)

        if self.record_images:
            out_file = self.record_image_folder + ("{:06d}.npy".format(self.i))
            with open(out_file, 'wb') as f:
                np.save(f, img)
        

        img -= 1000#np.nanmin(img)
        img = (img * 180) / 20000#np.nanmax(img) 
        new_img = 160 * np.ones((img.shape[0], img.shape[1], 3), dtype = np.uint8)  
        new_img[:,:,0] = img
        new_img = new_img.astype(np.uint8)
        bgr_img = cv2.cvtColor(new_img, cv2.COLOR_HSV2BGR)
        
        print("got image with mean", np.nanmean(img) )

        cv2.imshow("Zed Img", cv2.resize(bgr_img, (960, 540)))

        cv2.imwrite("./depth_img.png", bgr_img)
        cv2.waitKey(50)

        self.i += 1


def main(args=None):
    record_image_folder = "/home/josh/ros_ws/src/qubo_laptop/src/gate_images/"

    rclpy.init(args=args)


    img_subscriber = ImgSubscriber(record_image_folder)

    rclpy.spin(img_subscriber)

    # Destroy the node explicitly
    # (optional - otherwise it will be done automatically
    # when the garbage collector destroys the node object)
    img_subscriber.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()
