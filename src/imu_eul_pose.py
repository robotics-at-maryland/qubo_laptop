import rclpy
from rclpy.node import Node

from geometry_msgs.msg import PoseWithCovarianceStamped, Vector3
import numpy as np
import math
from scipy.spatial.transform import Rotation as R

class MinimalSubscriber(Node):

    def __init__(self):
        super().__init__('imu_eul_pose')
        self.subscription = self.create_subscription(
            PoseWithCovarianceStamped,
            '/vectornav/pose',
            self.listener_callback,
            5)
        self.publisher_ = self.create_publisher(Vector3, 'rpy', 5)
        self.subscription  # prevent unused variable warning
        #self.offset_quat = euler_to_quaternion(-155, 0, 0)
        self.offset_rot = R.from_euler('XYZ', [24.72, 179.59, 0], degrees = True)
        self.offset_rot *= R.from_euler('zyx', [90, 0, 0], degrees = True)

    def listener_callback(self, msg):
        quat = msg.pose.pose.orientation
        rot = R.from_quat([quat.x, quat.y, quat.z, quat.w])
        rot_t = rot * self.offset_rot
        eul = rot.as_euler('xyz', degrees=True)
        eul_t = rot_t.as_euler('xyz', degrees=True)

        #offset to fit our frame of reference
        eul_t[0] *= -1 #reverse roll
        eul_t[2] -= 0.0 #add offset to yaw for approximate north
        print("roll: %.2f, pitch: %.2f, yaw: %.2f" % (eul[0], eul[1], eul[2]))
        print("roll: %.2f, pitch: %.2f, yaw: %.2f [TRANSFORMED]" % (eul_t[0], eul_t[1], eul_t[2]))
        
        rpyMessage = Vector3()
        rpyMessage.x = eul_t[0]
        rpyMessage.y = eul_t[1]
        rpyMessage.z = eul_t[2]
        self.publisher_.publish(rpyMessage)



def main(args=None):
    rclpy.init(args=args)

    minimal_subscriber = MinimalSubscriber()

    rclpy.spin(minimal_subscriber)

    # Destroy the node explicitly
    # (optional - otherwise it will be done automatically
    # when the garbage collector destroys the node object)
    minimal_subscriber.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()
