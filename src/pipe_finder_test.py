import numpy as np
import cv2
import matplotlib.pyplot as plt

def remove_nan_smooth(img):

    while(True):
        inf_indices = np.argwhere(np.isinf(img[1:-1,1:-1]))
        print(len(inf_indices))

        if(len(inf_indices) == 0):
            break

        indices_stack = np.tile(inf_indices, (9,1,1))
        
        for i in range(3):
            for j in range(3):
                index = 3 * i + j
                indices_stack[index, :, 0] += i - 1
                indices_stack[index, :, 1] += j - 1
        
        taken_values = img[indices_stack[:,:,0] + 1, indices_stack[:,:,1] + 1]
        not_infs = np.invert(np.isinf(taken_values))
        replaceable_indexes = np.any(not_infs, axis=0)
        replaceable_pixels = indices_stack[4][replaceable_indexes] #get the middle pixel indices
        sum_len = np.sum(not_infs, axis=0)
        sums = np.sum(np.nan_to_num(taken_values, nan=0, posinf=0, neginf=0), axis=0)
        img[replaceable_pixels[:, 0] + 1, replaceable_pixels[:, 1] + 1] = sums[replaceable_indexes] / sum_len[replaceable_indexes]

    top_row_infs = np.argwhere(np.isinf(img[0])) 
    img[0, top_row_infs] = img[1, top_row_infs]

    bottom_row_infs = np.argwhere(np.isinf(img[-1])) 
    img[-1, bottom_row_infs] = img[-2, bottom_row_infs]

    left_col_infs = np.argwhere(np.isinf(img[:,0])) 
    img[left_col_infs, 0] = img[left_col_infs, 1]

    right_col_infs = np.argwhere(np.isinf(img[:,-1])) 
    img[right_col_infs, 0] = img[right_col_infs, 1]
    return img

def draw_lines(img, lines, color = (0,255,0)):
    first = True
    for r_theta in lines:
        arr = np.array(r_theta[0], dtype=np.float64)
        r, theta = arr
        a = np.cos(theta)
        b = np.sin(theta)
        x0 = a*r
        y0 = b*r
        x1 = int(x0 + 1000*(-b))
        y1 = int(y0 + 1000*(a))
        x2 = int(x0 - 1000*(-b))
        y2 = int(y0 - 1000*(a))
        if first:
            cv2.line(img, (x1, y1), (x2, y2), color, 2)
            first = False
        else:
            cv2.line(img, (x1, y1), (x2, y2), (255,255,255), 2)
    return img

def check_pipe(left_line, right_line, img, draw_img = None, angle_diff_thresh = 0.15, hori_fov = 120):
    if left_line is None or right_line is None:
        return None
    
    
    l_r, l_theta = left_line[0]
    r_r, r_theta = right_line[0]
    if np.abs(l_theta - r_theta) % 3.14 > angle_diff_thresh:
        return None
    
    mid_row = img.shape[0]//2
    print(img.shape)

    a_l = np.cos(l_theta)
    b_l = np.sin(l_theta)
    x0_l = a_l*l_r
    y0_l = b_l*l_r

    a_r = np.cos(r_theta)
    b_r = np.sin(r_theta)
    x0_r = a_r*r_r
    y0_r = b_r*r_r

    mid_x_l = x0_l + ((mid_row - y0_l) * -np.tan(l_theta))#+ np.abs((mid_row - y0_l) * b_l)
    mid_x_r = x0_r + ((mid_row - y0_r) * -np.tan(r_theta))

    depth = np.median(img[mid_row, (int)(mid_x_l): (int)(mid_x_r) + 1])
    print("depth", depth)

    cv2.circle(draw_img, ((int)(mid_x_l), mid_row), 10, (0,255,0), -1)
    cv2.circle(draw_img, ((int)(mid_x_r), mid_row), 10, (0,255,0), -1)

    xAngle = ((((mid_x_l + mid_x_r) / 2) - (img.shape[1]/2)) / img.shape[1]) * hori_fov

    return (xAngle, depth)

def vertEdgeDetect(img, blur=10, nms_length = 3, thresh = 200, min_length = 150):

    blurred_img = cv2.blur(img, (blur,blur))
    kernel1 = np.array([[-1, 0, 1]])
    hori_img_grad = cv2.filter2D(blurred_img, ddepth=-1, kernel=kernel1)
    nms_img = np.zeros((hori_img_grad.shape[0],hori_img_grad.shape[1],nms_length))
    widthNMS = hori_img_grad.shape[1] - 2 * (nms_length//2)
    for i in range(nms_length):
        x = i - nms_length//2
        nms_img[:,(nms_length//2) : (nms_length//2) + widthNMS, i] = hori_img_grad[:,x + (nms_length//2) : x + (nms_length//2) + widthNMS]
    
    max_arg = np.argmax(nms_img, axis=2)
    bool_edges_right = np.logical_and((max_arg == nms_length//2), (hori_img_grad > thresh)).astype(np.uint8)

    min_arg = np.argmin(nms_img, axis=2)
    bool_edges_left = np.logical_and((min_arg == nms_length//2), (hori_img_grad < -thresh)).astype(np.uint8)


    right_lines = cv2.HoughLines(bool_edges_right,2,np.pi/90, min_length)
    left_lines = cv2.HoughLines(bool_edges_left,2,np.pi/90, min_length)
    return left_lines, right_lines



if __name__ == "__main__":
    img_num = 0
    while(True):
        key = -1
        img_path = "/home/josh/ros_ws/src/qubo_laptop/src/pipe_images/{:06d}.npy".format(img_num)
        with open(img_path, "rb") as f:
            img = np.load(f)
            img = np.nan_to_num(img, nan=0, posinf=0, neginf=0)#img = remove_nan_smooth(img)

            left_lines, right_lines = vertEdgeDetect(img)

            print("lines below")
            if right_lines is not None:
                for line in right_lines:
                    print(line)

            draw_img = cv2.cvtColor((255 * (img) / np.max(img)).astype(np.uint8), cv2.COLOR_GRAY2BGR)

            right_line = left_line = None

            if right_lines is not None:
                draw_img = draw_lines(draw_img, right_lines, color=(0,0,255))
                right_line = right_lines[0]
            if left_lines is not None:
                draw_img = draw_lines(draw_img, left_lines, color=(255,0,0))
                left_line = left_lines[0]

            pipe = check_pipe(left_line, right_line, img, draw_img = draw_img)

            if pipe is not None:
                print("Pipe found, xAngleRel = ", pipe[0], " depth = ", pipe[1])

            cv2.imshow("pipe detector", draw_img)
            while key is not ord("n"):
                key = cv2.waitKey(30)
                if key is ord("q"):
                    exit()
        img_num += 1